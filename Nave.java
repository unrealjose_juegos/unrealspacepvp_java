public class Nave {
	
	//----Creacion--
	
	public Nave(){
		Nombre = "Nave desconocida";
		Vida = 100;
		DañoLaser = 25;
		DañoCañon = 25;
		DañoMisil = 25;
		DefensaEscudo = 25;
		DefensaEvasion = 25;
		DefensaLaser = 25;
		Victorias = 0;
		Derrotas = 0;
	}
	
	//----Estadisticas--
	
	private double Vida;
	private double DañoLaser,DañoCañon,DañoMisil;
	private double DefensaEvasion,DefensaEscudo,DefensaLaser;
	private String Nombre;
	private int Victorias,Derrotas;
	
	//----Nombre--
	
	public String getNombre(){
		return Nombre;
	}
	
	public void setNombre(String Nombre){
		this.Nombre = Nombre;
	}
	
	//----Vida--
	
	public double getVida(){
		return Vida;
	}
	
	//----Armas y Defensas--
	
	public double getArmaLaser(){
		return DañoLaser;
	}
	
	public double getArmaCañon(){
		return DañoCañon;
	}
	
	public double getArmaMisil(){
		return DañoMisil;
	}
	
	public double getDefensaEscudo(){
		return DefensaEscudo;
	}
	
	public double getDefensaEvasion(){
		return DefensaEvasion;
	}
	
	public double getDefensaLaser(){
		return DefensaLaser;
	}
	
	//Estadisticas
	
	public int Victorias(){
	    return Victorias;
	}
	
	public int Derrotas(){
	    return Derrotas;
	}
	
	public void PartidaGanada(){
	    Victorias++;
	}
	
	public void PartidaPerdida(){
	    Derrotas++;
	}
	
	public int Estadisticas(){
	    System.out.println("Victorias de "+getNombre()+": "+Victorias);
	    System.out.println("Derrotas de "+getNombre()+": "+Derrotas);
	}
	
}