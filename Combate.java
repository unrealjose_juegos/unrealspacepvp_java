import java.io.Console;

public class Combate {

	public void Batalla(Nave Jugador1,Nave Jugador2){
		
		double DañoJugador1,EscudoJugador1,DañoJugador2,EscudoJugador2;
		int ArmaJugador1,DefensaJugador1,ArmaJugador2,DefensaJugador2;
		int turno = 0;
		boolean salir = true;
		
		double VidaJugador1,VidaJugador2;
		
		VidaJugador1 = 	Jugador1.getVida();
		VidaJugador2 = Jugador2.getVida();
		
		do{
			
			turno++;
			System.out.println();
			System.out.println("#########\n#Turno "+turno+"#\n#########");
			
			System.out.println();
			System.out.println("###################################################################");
			System.out.println("Nombre: "+Jugador1.getNombre()+"\nVida: ("+VidaJugador1+"/"+Jugador1.getVida()+")");
			System.out.println();
			System.out.println("Nombre: "+Jugador2.getNombre()+"\nVida: ("+VidaJugador2+"/"+Jugador2.getVida()+")");
			System.out.println("###################################################################");
			
			System.out.println("Turno de "+Jugador1.getNombre());
			int armaJugador1 = ElegirArma();
			
			switch(armaJugador1){
				case 1:
					DañoJugador1 = Jugador1.getArmaLaser();
					ArmaJugador1 = 1;
					break;
				case 2:
					DañoJugador1 = Jugador1.getArmaCañon();
					ArmaJugador1 = 2;
					break;
				case 3:
					DañoJugador1 = Jugador1.getArmaMisil();
					ArmaJugador1 = 3;
					break;
				default:
					DañoJugador1 = 0;
					ArmaJugador1 = 0;
					break;
			}
			
			int defensaJugador1 = ElegirDefensa();
			
			switch(defensaJugador1){
				case 1:
					EscudoJugador1 = Jugador1.getDefensaEscudo();
					DefensaJugador1 = 1;
					break;
				case 2:
					EscudoJugador1 = Jugador1.getDefensaEvasion();
					DefensaJugador1 = 2;
					
					break;
				case 3:
					EscudoJugador1 = Jugador1.getDefensaLaser();
					DefensaJugador1 = 3;
					break;
				default:
					DefensaJugador1 = 0;
					EscudoJugador1 = 0;
					break;
			}
			
			System.out.println("Turno de "+Jugador1.getNombre());
			int armaJugador2 = ElegirArma();
			
			switch(armaJugador2){
				case 1:
					DañoJugador2 = Jugador2.getArmaLaser();
					ArmaJugador2 = 1;
					break;
				case 2:
					DañoJugador2 = Jugador2.getArmaCañon();
					ArmaJugador2 = 2;
					break;
				case 3:
					DañoJugador2 = Jugador2.getArmaMisil();
					ArmaJugador2= 3;
					break;
				default:
					DañoJugador2 = 0;
					ArmaJugador2 = 0;
					break;
			}
			
			int defensaJugador2 = ElegirDefensa();
			
			switch(defensaJugador2){
				case 1:
					EscudoJugador2 = Jugador2.getDefensaEscudo();
					DefensaJugador2 = 1;
					break;
				case 2:
					EscudoJugador2 = Jugador2.getDefensaEvasion();
					DefensaJugador2 = 2;
					break;
				case 3:
					EscudoJugador2 = Jugador2.getDefensaLaser();
					DefensaJugador2 = 3;
					break;
				default:
					DefensaJugador2 = 0;
					ArmaJugador2 = 0;
					break;
			}
	
			if(ArmaJugador1 == 0) System.out.println(Jugador1.getNombre()+" no hace nada");
			if(ArmaJugador1 == 1) System.out.println(Jugador1.getNombre()+" dispara los laser");
			if(ArmaJugador1 == 2) System.out.println(Jugador1.getNombre()+" dispara los cañones");
			if(ArmaJugador1 == 3) System.out.println(Jugador1.getNombre()+" dispara los misiles");
			
			if(DefensaJugador2 == 0) System.out.println(Jugador2.getNombre()+" olvido activar las defensas");
			if(DefensaJugador2 == 1) System.out.println(Jugador2.getNombre()+" activa los escudos");
			if(DefensaJugador2 == 2) System.out.println(Jugador2.getNombre()+" activa el modulo de evasion");
			if(DefensaJugador2 == 3) System.out.println(Jugador2.getNombre()+" activa los sistemas antimisiles");
			
			if (ArmaJugador1 == DefensaJugador2){
				System.out.println(Jugador2.getNombre()+" bloqueo el ataque");
			}else{
				System.out.println(Jugador1.getNombre()+" infligio "+DañoJugador1+" de daño a "+Jugador2.getNombre());
				VidaJugador2-=DañoJugador1;
			}
			
			//---Muerte Jugador2------
			if(VidaJugador2<=0){
				System.out.println(Jugador2.getNombre()+" ha sido eliminado por "+Jugador1.getNombre());
				Jugador1.PartidaGanada();
				Jugador2.PartidaPerdida();
				salir = false;
			}
			//-----------------------
			
			if(salir){
			    System.out.println();
			
			    if(ArmaJugador2 == 0) System.out.println(Jugador2.getNombre()+" no hace nada");
			    if(ArmaJugador2 == 1) System.out.println(Jugador2.getNombre()+" dispara los laser");
			    if(ArmaJugador2 == 2) System.out.println(Jugador2.getNombre()+" dispara los cañones");
			    if(ArmaJugador2 == 3) System.out.println(Jugador2.getNombre()+" dispara los misiles");
			
			    if(DefensaJugador1 == 0) System.out.println(Jugador1.getNombre()+" olvido activar las defensas");
			    if(DefensaJugador1 == 1) System.out.println(Jugador1.getNombre()+" activa los escudos");
			    if(DefensaJugador1 == 2) System.out.println(Jugador1.getNombre()+" activa el modulo de evasion");
			    if(DefensaJugador1 == 3) System.out.println(Jugador1.getNombre()+" activa los sistemas antimisiles");
						
		        if (ArmaJugador2 == DefensaJugador1){
			    	System.out.println(Jugador1.getNombre()+" bloqueo el ataque");
		    	}else{
			    	System.out.println(Jugador2.getNombre()+" infligio "+DañoJugador2+" de daño a "+Jugador1.getNombre());
		    		VidaJugador1-=DañoJugador2;
		    	}
		    	
		    	//----Muerte Jugador1-----
		    	if(VidaJugador1<=0){
		    		System.out.println(Jugador1.getNombre()+" ha sido eliminado por "+Jugador2.getNombre());
		    		Jugador2.PartidaGanada();
			    	Jugador1.PartidaPerdida();
			    	salir = false;
			    }   
			}
			
		}while(salir); 
		
	}
	
	public int ElegirArma(){
	
	    Console terminal = System.console();
		if (terminal==null ) {
			System.err.println("No puedes usar estre prograna desde la consola de un IDE, usa la Terminal.");
			return 0;
		}
        
        System.out.println("1 -> Laser\n2 -> Cañon\n3 -> Misil");
		String eleccion2 = new String (terminal.readPassword("Password:"));
		int eleccion = Integer.parseInt(eleccion2);
				
		if(eleccion>4 & eleccion<=0){
			eleccion = ElegirArma();
		}    
	    
		return eleccion;
	}
	
	public int ElegirDefensa(){
	
        Console terminal = System.console();
	   	if (terminal==null ) {
	    	System.err.println("No puedes usar estre prograna desde la consola de un IDE, usa la Terminal.");
	    	return 0;
	   	}
    
        System.out.println("1 -> Evasion\n2 -> Escudo\n3 -> Laser");
	   	String eleccion2 = new String (terminal.readPassword("Password:"));
	    int eleccion = Integer.parseInt(eleccion2);

	   	if(eleccion>4 & eleccion<=0){
	    	eleccion = ElegirDefensa();
	   	}    
	    
	    return eleccion;
	}
	
}