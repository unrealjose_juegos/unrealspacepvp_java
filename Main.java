import java.util.Scanner;

public class Main {
	
	/*
	 * Version: 0.0.1
	 */

	public static void main(String[] args) {
		
		//----Variables--
		Nave Jugador1 = new Nave();
		Nave Jugador2 = new Nave();
		Combate Combate = new Combate();
		Scanner sc = new Scanner(System.in);
		String NombreJ1,NombreJ2;
		boolean salir=true;
		
		//----Introduccion--
		System.out.println("¿Como se llama la nave del Jugador 1?");
		NombreJ1 = sc.nextLine();
		Jugador1.setNombre(NombreJ1);
		System.out.println("¿Como se llama la nave del Jugador 2?");
		NombreJ2 = sc.nextLine();
		Jugador2.setNombre(NombreJ2);
		
		//----Menu--
		while(true){
		    
		    do{
		      System.out.println("Menu:\n1 -> Combate\n2 -> Estadisticas(En desarollo)");
		      try{
		           int opcion = sc.nextInt(); 
		           salir = false;
		      }catch(Exception e){
		          salir = true;
		      }finally{
		          if(opcion!=1 || opcion!=2)
		            salir = true;
		      }
		    }while(salir)
			
			switch(opcion){
				case 1:
					Combate.Batalla(Jugador1, Jugador2);
					break;
				case 2:
				    Jugador1.Estadisticas();
				    Jugador2.Estadisticas();
				    break;
				default:
					break;
			}
			
		}

	}

}